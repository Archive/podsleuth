AC_DEFUN([PODSLEUTH_CHECK_NDESK_DBUS],
[
	PKG_CHECK_MODULES(NDESK_DBUS, \
		ndesk-dbus-1.0 >= 0.4)

	dnl work around a bug in the .pc file
	MONO_PREFIX=$($PKG_CONFIG --variable=prefix mono)
	NDESK_DBUS_LIBS=$(echo $NDESK_DBUS_LIBS | sed "s,@prefix@,$MONO_PREFIX,g")

	AC_SUBST(NDESK_DBUS_LIBS)
])

AC_DEFUN([PODSLEUTH_CHECK_HAL],
[
	PKG_CHECK_MODULES(HAL, hal >= $1)

	AC_ARG_WITH(hal_callouts_dir, AC_HELP_STRING([--with-hal-callouts-dir=DIR],
		[Directory where HAL expects its callout scripts to be located]), , 
		with_hal_callouts_dir="")

	if test "x$with_hal_callouts_dir" = "x"; then
		HALCALLOUTSDIR="${libdir}/hal"
		HALCALLOUTSDIR_EXPANDED="${expanded_libdir}/hal"
	else
		HALCALLOUTSDIR="$with_hal_callouts_dir"
		HALCALLOUTSDIR_EXPANDED="$with_hal_callouts_dir"
	fi

	AC_SUBST(HALCALLOUTSDIR)
	AC_SUBST(HALCALLOUTSDIR_EXPANDED)
])

AC_DEFUN([PODSLEUTH_CHECK_SGUTILS],
[
	LIBSGUTILS_SO=NONE
	
	AC_CHECK_LIB(sgutils2, sg_ll_inquiry,
		[LIBSGUTILS_SO=libsgutils2.so.2], [])
	
	if test "x$LIBSGUTILS_SO" = "xNONE"; then
		AC_CHECK_LIB(sgutils, sg_ll_inquiry,
			[LIBSGUTILS_SO=libsgutils.so.1], [])
	fi

	if test "x$LIBSGUTILS_SO" = "xNONE"; then
		AC_MSG_ERROR([Error! You need to have libsgutils.])
	fi

	AC_SUBST(LIBSGUTILS_SO)
])

AC_DEFUN([PODSLEUTH_CHECK_UPDATE_DIR],
[
    case $prefix in
		NONE) prefix=$ac_default_prefix ;;
		*) ;;
	esac

	case $prefix in
		/usr) default_update_dir="/var/cache/podsleuth" ;;
		*) default_update_dir="$prefix/var/cache/podsleuth" ;;
	esac

	AC_ARG_WITH(update_dir, AC_HELP_STRING([--with-update-dir=DIR],
		[Directory where PodSleuth updates should be stored]),
		[], [with_update_dir="$default_update_dir"])

	UPDATE_DIR="$with_update_dir"
	AC_SUBST(UPDATE_DIR)
])

