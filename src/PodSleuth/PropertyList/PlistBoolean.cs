using System;

namespace PropertyList
{
    public class PlistBoolean : PlistObject<bool>
    {
        public PlistBoolean(bool value) : base(value)
        {
        }
        
        public override void Dump(int padding)
        {
            WriteLine(padding, "<{0}/>", Value ? "true" : "false");
        }
    }
}
