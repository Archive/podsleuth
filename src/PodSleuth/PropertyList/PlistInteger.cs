using System;

namespace PropertyList
{
    public class PlistInteger : PlistObject<int>
    {
        public PlistInteger(int value) : base(value)
        {
        }
        
        public override void Dump(int padding)
        {
            WriteLine(padding, "<integer>{0}</integer>", Value);
        }
    }
}
