using System;
using System.Text;
using System.IO;
using System.Collections.Generic;

using PodSleuth.ModelData;
using PropertyList;

namespace PodSleuth
{
    public class Device
    {
        private string block_device;
        private string mount_point;
        
        private PlistDocument plist;
        private PlistDictionary plist_dict;
        private ModelInformation model;
        private DevicePaths paths;
        private SysInfo sysinfo;
        private SerialNumber serial_number;
        private DeviceCapabilities capabilities = DeviceCapabilities.None;
        private string firmware_version;
        private string firewire_id;
        private List<ImageFormat> image_formats = new List<ImageFormat>();
        private bool photos_supported = false;
        private bool album_art_supported = false;
        private bool sparse_album_art_supported = false;
        private bool chapter_images_supported = false;
        private bool is_shuffle = false;
        
        public Device(string blockDevice, string mountPoint)
        {
            this.block_device = blockDevice;
            this.mount_point = mountPoint;
        }
        
        public override string ToString ()
        {
            StringBuilder builder = new StringBuilder ();

            builder.Append ("[Paths]");
            builder.AppendLine ();
            builder.Append (paths);
            builder.AppendLine ();

            builder.Append ("[SysInfo]");
            builder.AppendLine ();
            if (sysinfo != null) {
                builder.Append (sysinfo);
            }
            builder.AppendLine ();

            builder.Append ("[Model]");
            builder.AppendLine ();
            builder.Append (model);
            builder.AppendLine ();

            return builder.ToString ();
        }

        public void Load()
        {
            FindPaths();
            LoadSysInfo();
            LoadDevicePlist();
            FindSerialNumber();
            
            if(!FindModel()) {
                return;
            }
            
            // TODO: The model information may be useful to the DevicePaths discoverer in
            // the future, but is a slight performance hit as we already discovered paths
            // before. This may be necessary for supporting devices like SLVR and ROKR
            // FindPaths();
        }
        
        private void FindPaths()
        {
            paths = new DevicePaths(mount_point, model);
        }
        
        private void LoadSysInfo()
        {
            try {
                sysinfo = new SysInfo(paths.SysInfoFile);
            } catch {
                try {
                    sysinfo = new SysInfo(Path.Combine (mount_point, paths.SysInfoFile.Substring (1)));
                } catch {
                    Console.WriteLine("** Could not read iPod SysInfo file **");
                    sysinfo = null;
                }
            }
        }
        
        private bool? ReadPlistBoolNullable(string key)
        {
            if(!plist_dict.ContainsKey(key)) {
                return null;
            }
            
            PlistObjectBase o = plist_dict[key];
            if(o is PlistBoolean) {
                return ((PlistBoolean)o).Value;
            }
            
            return null;
        }
        
        private bool ReadPlistBool(string key)
        {
            bool? value = ReadPlistBoolNullable(key);
            return value == null ? false : value.Value;
        }
        
        private void LoadDevicePlist()
        {
            try {
                plist = new PlistDocument();
                plist.LoadFromXml(ScsiReader.ReadPlist(BlockDevice));
                plist_dict = (PlistDictionary)plist.Root;
            } catch(Exception e) {
                Console.WriteLine("** Could not read iPod property list from SCSI code page **");
                Console.WriteLine(e);
                plist_dict = null;

                if (sysinfo == null) {
                    Console.WriteLine("** Could not read iPod properties **");
                } else {
                    // Fallback to get 3rd gen iPods working, see http://bugzilla.gnome.org/show_bug.cgi?id=520516
                    Console.WriteLine("** Reading properties from SysInfo **");

                    try {
                        string version = sysinfo["visibleBuildID"];
                        firmware_version = version.Substring(3, 1) + '.' + version.Substring(4, 1);
                        if (version.Substring(5, 1) != "0") {
                            firmware_version = firmware_version + '.' + version.Substring(5, 1);
                        }

                        string fwid = sysinfo["FirewireGuid"].Substring(2, 8);
                        firewire_id = "000A2700" + fwid;
                    } catch {}
                }

                return;
            }
            
            if(ReadPlistBool("PodcastsSupported")) {
                capabilities |= DeviceCapabilities.Podcast;
            } 
            
            if(ReadPlistBool("iCalendarsSupported")) {
                capabilities |= DeviceCapabilities.ICalendar;
            }
            
            if(ReadPlistBool("vCardsSupported")) {
                capabilities |= DeviceCapabilities.VCard;
            }

            if(ReadPlistBool("SupportsSparseArtwork")) {
                sparse_album_art_supported = true;
            }
            
            if(plist_dict.ContainsKey("VisibleBuildID")) {
                firmware_version = ((PlistString)plist_dict["VisibleBuildID"]).Value;
            }
            
            if(plist_dict.ContainsKey("ShadowDB")) {
                is_shuffle = ((PlistBoolean)plist_dict["ShadowDB"]).Value;
            }
            
            if(plist_dict.ContainsKey("FireWireGUID")) {
                firewire_id = ((PlistString)plist_dict["FireWireGUID"]).Value;
            }

            foreach(string img_class in new string [] { "ImageSpecifications", "AlbumArt", "ChapterImageSpecs" }) {
                if(!plist_dict.ContainsKey(img_class)) {
                    continue;
                }

                bool added_any = false;
                
                PlistDictionary formats = plist_dict[img_class] as PlistDictionary;
                if(formats != null && formats.Count > 0) {
                    foreach(KeyValuePair<string, PlistObjectBase> kvp in formats) {
                        try {
                            ImageFormat format = new ImageFormat((PlistDictionary)kvp.Value, img_class);
                            image_formats.Add(format);
                            added_any = true;
                        } catch {
                        }
                    }
                } else {
                    // In 4th gen Nanos, the plist format was changed to list the image
                    // types supported in an array instead of as entries in a dict.
                    PlistArray formats_array = plist_dict[img_class] as PlistArray;
                    if (formats_array != null) {
                        foreach(PlistObjectBase val in formats_array) {
                            try {
                                ImageFormat format = new ImageFormat((PlistDictionary)val, img_class);
                                image_formats.Add(format);
                                added_any = true;
                            } catch {
                            }
                        }
                    }
                }

                if(!added_any) {
                    continue;
                }
                
                switch(img_class) {
                    case "ImageSpecifications": photos_supported = true; break;
                    case "AlbumArt": album_art_supported = true; break;
                    case "ChapterImageSpecs": chapter_images_supported = true; break;
                }
            }
        }
        
        private void FindSerialNumber()
        {            
            try {
                serial_number = SerialNumber.Parse(ScsiReader.ReadSerial(BlockDevice));
            } catch(Exception e) {
                Console.WriteLine("** Could not read serial number from SCSI code page **");
                Console.WriteLine(e);
            }
            
            if(serial_number.Equals(SerialNumber.Zero)) {
                if(plist_dict != null) {
                    serial_number = SerialNumber.Parse(((PlistString)plist_dict["SerialNumber"]).Value);
                }
                
                if(serial_number.Equals(SerialNumber.Zero) && sysinfo != null) {
                    serial_number = SerialNumber.Parse(sysinfo["pszSerialNumber"]);
                }
            }
        }
        
        private string FindModelNumber()
        {
            if(sysinfo == null) {
                return null;
            }
            
            string model = sysinfo["ModelNumStr"];
            if (model != null && model.Length >= 5) {
                model = model.Substring(1);
                model = model.Substring(0, 4);
            }

            return model;
        }
        
        private bool FindModel()
        {
            ModelTable table = new ModelTable();
            
            bool loaded_update = false;
            
            try {
                if(System.IO.File.Exists(ModelTable.UpdateTableFile)) {
                    table.Load(ModelTable.UpdateTableFile);
                    loaded_update = true;
                }
            } catch {
            }
            
            if(!loaded_update) {
                table.Load(ResourceUtilities.GetFileContents("ipod-model-table"));
            }
            
            model = table.LookupBySerialCode(serial_number.ModelCode);
            if(model == null) {
                model = table.LookupByModelNumber(FindModelNumber());
            }
            
            return model != null;
        }
        
        public string BlockDevice {
            get { return block_device; }
            set { block_device = value; }
        }
        
        public string MountPoint {
            get { return mount_point; }
            set { mount_point = value; }
        }
        
        public SerialNumber SerialNumber {
            get { return serial_number; }
        }
        
        public bool IsUnknown {
            get { return model == null; }
        }
        
        public ModelInformation ModelInformation {
            get { return model; }
        }
        
        public DevicePaths Paths {
            get { return paths; }
        }
        
        public PlistDocument PlistDocument {
            get { return plist; }
        }
        
        public DeviceCapabilities Capabilities {
            get { return capabilities; }
        }
        
        public string FirewireID {
            get { return firewire_id; }
        }
        
        public string FirmwareVersion {
            get { return firmware_version; }
        }
        
        public bool PhotosSupported {
            get { return photos_supported; }
        }
        
        public bool AlbumArtSupported {
            get { return album_art_supported; }
        }

        public bool SparseAlbumArtSupported {
            get { return sparse_album_art_supported; }
        }
        
        public bool ChapterImagesSupported {
            get { return chapter_images_supported; }
        }
        
        public IList<ImageFormat> ImageFormats {
            get { return image_formats; }
        }
        
        public bool IsShuffle {
            get { return is_shuffle; }
        }
    }
}
