using System;

namespace PodSleuth
{
    [Flags]
    public enum DeviceCapabilities
    {
        None      = 0,
        Podcast   = 1 << 0,
        ICalendar = 1 << 1,
        VCard     = 1 << 2        
    }
}
