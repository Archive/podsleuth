using System;

namespace PodSleuth
{
    public struct SerialNumber
    {
        // Apple Serial Number Parser
        //
        // Format: FFYWWNNNCCC
        // Where:
        //   FF  - String:         ID of production factory (5C)
        //   Y   - Base 10 Int:    Production year, since 2000 (7)
        //   WW  - Base 10 Int:    Production week, since start of year (07)
        //   NNN - Base 36 Int:    Production index, since start of week (6FV)
        //   CCC - String:         ID of model, used to identify device class, capacity,
        //                         and shell color; key in model table (VTE)
        //
        // Example: 5C7076FVVTE
        // Where:
        //   5C  - The factory ID that produced the iPod
        //   7   - Produced in 2007
        //   07  - In week 7 of 2007
        //   6FV - Was 7477th iPod to be produced in that week
        //   VTE - Which was a silver second generation 1GB shuffle
        
        public static SerialNumber Zero;
        
        private string serial_number;
        private string model_code;
        
        private string production_factory_id;
        private int production_year;
        private int production_week;
        private int production_number;
        
        public static SerialNumber Parse(string serialNumber)
        {
            if(serialNumber == null) {
                return Zero;
            }
            
            string serial = serialNumber.Trim();
            
            if(serial.Length != 11) {
                return Zero;
            }
            
            SerialNumber serial_number = new SerialNumber();
            
            serial_number.serial_number = serial;
            serial_number.model_code = serial.Substring(serial.Length - 3);
            
            serial_number.production_factory_id = serial.Substring(0, 2);
            serial_number.production_year = (serial[2] - '0') + 2000;
            serial_number.production_week = (serial[3] - '0') * 10 + (serial[4] - '0');
            
            try {
                serial_number.production_number = (int)BaseConverter.ToBase10(serial.Substring(5, 3), 36);
            } catch {
                serial_number.production_number = 0;
            }
            
            return serial_number;
        }
        
        public override string ToString()
        {
            return String.Format("iPod '{0},' {1} in week {2} of year {3} from factory {4} ({5})",
                ModelCode, ProductionNumber, ProductionWeek, ProductionYear, 
                ProductionFactoryID, WholeSerialNumber);
        }
        
        public string ModelCode {
            get { return model_code; }
        }
        
        public string WholeSerialNumber {
            get { return serial_number; }
        }
        
        public string ProductionFactoryID {
            get { return production_factory_id; }
        }
        
        public int ProductionYear {
            get { return production_year; }
        }
        
        public int ProductionWeek {
            get { return production_week; }
        }
        
        public int ProductionNumber {
            get { return production_number; }
        }
    }
}
