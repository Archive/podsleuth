using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace PodSleuth
{
    public class SysInfo
    {
        private static Dictionary<string, string> sysinfo = new Dictionary<string, string>();
        
        public SysInfo()
        {
        }
        
        public SysInfo(string path)
        {
            Load(path);
        }
        
        public void Load(string path)
        {
            using(StreamReader reader = File.OpenText(path)) {
                string line;
                while((line = reader.ReadLine()) != null) {
                    LoadLine(line.Trim());
                }
            }
        }
        
        private void LoadLine(string line)
        {
            string [] parts = line.Split(new char [] { ':' }, 2);
            
            if(parts.Length != 2) {
                return;
            }
            
            string key = parts[0].Trim();
            string value = parts[1].Trim();
            
            if(value.StartsWith("0x")) {
                parts = value.Split(' ');
                value = parts[0];
            }
            
            sysinfo.Add(key, value);
        }
        
        public string this[string key] {
            get {
                if(sysinfo.ContainsKey(key)) {
                    return sysinfo[key];
                }
                
                return null;
            }
        }
    }
}
