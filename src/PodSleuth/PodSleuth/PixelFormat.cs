using System;

namespace PodSleuth
{
    public enum PixelFormat
    {
        Unknown,
        Rgb565,
        Rgb565_BE,
        Iyuv
    }
}
