using System;
using System.IO;
using System.Runtime.InteropServices;

using Mono.Unix;
using Mono.Unix.Native;

namespace PodSleuth
{
    public static class LowLevelMount
    {
        [DllImport("libc")]
        private static extern int mount(string source, string target, 
            string fstype, uint mountflags, IntPtr data);
        
        [DllImport("libc")]
        private static extern int umount(string target);
        
        public static void Mount(string source, string target, string fstype, bool readOnly)
        {
            if(mount(source, target, fstype, (uint)(readOnly ? 1 : 0), IntPtr.Zero) == -1) {
                throw new UnixIOException(Stdlib.GetLastError());
            }
        }        
        
        public static void Umount(string target)
        {
            if(umount(target) == -1) {
                throw new UnixIOException(Stdlib.GetLastError());
            }
        }
        
        public static string GenerateMountPoint()
        {
            string prefix = "/tmp/podsleuth-mount";
            int index = 0;
            
            while(true) {
                string directory = String.Format("{0}-{1}", prefix, index++);
                if(Directory.Exists(directory)) {
                    continue;
                }
                
                Directory.CreateDirectory(directory);
                return directory;
            }
        }
    }
}
