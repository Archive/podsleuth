using System;

namespace PodSleuth
{
    public enum ImageType
    {
        Unknown,
        Photo,
        Album,
        Chapter
    }
}
