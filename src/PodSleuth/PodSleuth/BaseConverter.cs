using System;
using System.Text;

namespace PodSleuth
{
    internal static class BaseConverter
    {
        private const string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static long ToBase10(string value, int fromRadix)
        {
            if(fromRadix < 2 || fromRadix > 36) {
                throw new ArgumentOutOfRangeException("fromRadix", "Radix must be from 2 to 36");
            }

            if(String.IsNullOrEmpty(value)) {
                throw new ArgumentNullException("value");
            }

            long result = 0;

            for(int max = value.Length - 1, index = max; index >= 0; index--) {
                int key = characters.IndexOf(value[index]);
               
                if(key < 0 || key >= fromRadix) {
                    throw new FormatException("Unsupported character in value string");
                }
                
                result += key * (long)Math.Pow(fromRadix, max - index);

                if(result < 0) {
                    throw new OverflowException();
                }
            }

            return result;
        }
    }
}
