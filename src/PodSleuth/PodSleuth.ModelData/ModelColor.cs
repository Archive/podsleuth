using System;

namespace PodSleuth.ModelData
{
    public enum ModelColor
    {
        White,
        Silver,
        Black,
        Red,
        Orange,
        Blue,
        Green,
        Pink,
        Gold,
        Purple,
        Yellow
    }
}
