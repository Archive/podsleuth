using System;
using System.IO;
using System.Web;
using System.Net;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace PodSleuth.ModelData
{
    public class ModelTable : IEnumerable<ModelInformation>
    {
        private Dictionary<string, ModelInformation> serial_table = new Dictionary<string, ModelInformation>();
        private Dictionary<string, ModelInformation> model_table = new Dictionary<string, ModelInformation>();
        private List<ModelInformation> models = new List<ModelInformation>();
        
#if MD_BUILD
        internal static string UpdateCacheDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
#else
        internal static string UpdateCacheDirectory = InstallPaths.CacheDirectory;
#endif
        internal static string UpdateTableFile = Path.Combine(UpdateCacheDirectory, "ipod-model-table");

        private enum Field {
            Class,
            Generation,
            Color,
            Capacity,
            ModelNumber,
            Attributes,
            SerialCodes
        }
        
        public ModelTable()
        {
        }
        
        public void Load(string path)
        {
            using(StreamReader reader = File.OpenText(path)) {
                Load(reader);
                reader.Close();
            }
        }
        
        public void Load(StreamReader reader)
        {
            string line;
            while((line = reader.ReadLine()) != null) {
                LoadModel(line.Trim());
            }
        }
        
        private void LoadModel(string line)
        {
            if(line.Length == 0 || line[0] == ';') {
                return;
            }
            
            string [] parts = Regex.Split(line, @"\s+");
            
            ModelInformation model = new ModelInformation();
            List<string> serials = new List<string>();
            
            if(parts.Length != (int)Field.SerialCodes + 1) {
                Console.WriteLine("Malformed model entry:");
                Console.WriteLine(line);
                throw new InvalidDataException("Malformed model entry");
            }
            
            for(Field field = Field.Class; field <= Field.SerialCodes && (int)field < parts.Length; field += 1) {
                string part = parts[(int)field].Trim();
                
                switch(field) {
                    case Field.Class: 
                        model.Class = StringToEnum<ModelClass>(part); 
                        break;
                    case Field.Generation: 
                        model.Generation = Convert.ToDouble(part); 
                        break;
                    case Field.Color: 
                        model.Color = StringToEnum<ModelColor>(part); 
                        break;
                    case Field.Capacity: 
                        model.Capacity = Convert.ToDouble(part); 
                        break;
                    case Field.ModelNumber: 
                        model.ModelNumber = part; 
                        break;
                    case Field.Attributes: 
                        foreach(string attr in part.Split(',')) {
                            if(attr != "None") {
                                model.Attributes |= StringToEnum<ModelAttributes>(attr);
                            }
                        }
                        break;
                    case Field.SerialCodes:
                        if(part == "Unknown") {
                            break;
                        }

                        foreach(string code in part.Split(',')) {
                            string _code = code.ToUpper();
                
                            serials.Add(_code);
                
                            try {
                                serial_table.Add(_code, model);
                            } catch(Exception e) {
                                Console.WriteLine("Serial code already exists: {0}", _code);
                                Console.WriteLine(model);
                                Console.WriteLine("------");
                                Console.WriteLine("Duplicate of:");
                                Console.WriteLine(serial_table[_code]);
                            
                                throw e;
                            }
                        }
                        break;
                }
            }
            
            if(serials.Count == 0) {
                model.SerialCodes = new string[0];
            } else {
                model.SerialCodes = serials.ToArray();
            }
            
            if(!model_table.ContainsKey(model.ModelNumber)) {
                model_table.Add(model.ModelNumber, model);
            } 
            
            models.Add(model);
        }   

        private T StringToEnum<T>(string value)
        {
            Type type = typeof(T);
            FieldInfo field = type.GetField(value);
            
            if(field == null) {
                throw new NullReferenceException(value);
            }
            
            return (T)field.GetValue(type);
        }
        
        public ModelInformation LookupBySerialCode(string id)
        {
            if(id == null) {
                return null;
            }
        
            if(serial_table.ContainsKey(id)) {
                return serial_table[id];
            }
            
            return null;
        }
        
        public ModelInformation LookupByModelNumber(string id)
        {
            if(id == null) {
                return null;
            }
            
            if(model_table.ContainsKey(id)) {
                return model_table[id];
            }
            
            return null;
        }
        
        public static void DownloadTableUpdate()
        {
            Directory.CreateDirectory(UpdateCacheDirectory);
            SaveHttpStream(new Uri("http://banshee-project.org/files/podsleuth/ipod-model-table"), UpdateTableFile);
        }
        
        private static Stream GetHttpStream(Uri uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri.AbsoluteUri);
            request.UserAgent = "PodSleuth";
            request.Timeout = 20 * 1000;
            request.KeepAlive = false;
            request.AllowAutoRedirect = true;
            return request.GetResponse().GetResponseStream();
        }
        
        private static void SaveHttpStream(Uri uri, string path)
        {
            Stream from_stream = GetHttpStream(uri);
            if(from_stream == null) {
                throw new IOException("Resource could not be reached: " + uri.AbsoluteUri);
            }
            
            using(from_stream) {
                long bytes_read = 0;

                using(FileStream to_stream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite)) {
                    byte [] buffer = new byte[8192];
                    int chunk_bytes_read = 0;

                    while((chunk_bytes_read = from_stream.Read(buffer, 0, buffer.Length)) > 0) {
                        to_stream.Write(buffer, 0, chunk_bytes_read);
                        bytes_read += chunk_bytes_read;
                    }
                }
            }
        }
        
        public IEnumerator<ModelInformation> GetEnumerator()
        {
            return models.GetEnumerator();
        }
        
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return models.GetEnumerator();
        }
    }
}
